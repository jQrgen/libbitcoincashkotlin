import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.io.FileInputStream
import java.util.Properties

val serializationVersion = "1.3.0-RC"
val junitVersion = "5.6.2"
val roomVersion = "2.4.0-beta02"

plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    kotlin("plugin.serialization")
    `maven-publish`
    id("org.jetbrains.dokka") version "1.5.30"
}

fun getVersionCode(): String {
    val majorMinorVersion = "3.0."
    val buildNumber = System.getenv("LIBBITCOINCASH_VERSION_CODE") ?: "999"
    return majorMinorVersion + buildNumber
}

fun readVersionCode(): Int? {
    // CI sets LIBBITCOINCASH_VERSION_CODE for the released version.
    System.getenv("LIBBITCOINCASH_VERSION_CODE")?.let {
        val versionCode = it.toInt()
        println("INFO: Using version code $versionCode")
        return versionCode
    }
    // Developer build
    return null
}

android {
    compileSdk = 31

    defaultConfig {
        testApplicationId = "info.bitcoinunlimited.libbitcoincash"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        testFunctionalTest = true

        minSdk = 26
        targetSdk = 31

        consumerProguardFiles("proguard-rules.pro")

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        externalNativeBuild {
            cmake {
                cppFlags.add("-std=c++17")
                targets.add("bitcoincashandroid")

                // Enable ring signature extension by adding 'ringsignatures=true'
                // to 'local.properties' file.
                val prop = Properties()
                try {
                    prop.load(FileInputStream("local.properties"))
                } catch (e: java.io.FileNotFoundException) {
                    println("INFO: local.properties does not exist.")
                }
                val enableRingsigProperty = prop.getProperty("ringsignatures")
                val enableRingsig = enableRingsigProperty != null &&
                    (enableRingsigProperty == "true" || enableRingsigProperty == "on")

                if (enableRingsig) {
                    println("Experimental ring signatures enabled.")

                    var ndkPath: String? = prop.getProperty("ndk.dir")
                    if (ndkPath == null) {
                        ndkPath = System.getenv("ANDROID_NDK_ROOT")
                    }
                    if (ndkPath == null) {
                        throw Exception(
                            "You need to set the ANDROID_NDK_ROOT environment " +
                                "variable, or `ndk.dir` in local.properties to " +
                                "point to your Android NDK"
                        )
                    }

                    targets.add("ringsigandroid")
                    arguments.add("-DENABLE_RINGSIG=ON")
                    arguments.add("-DNDK_ROOT=" + ndkPath)
                }

                // TODO: Should this be be cFlags instead of arguments?
                // See: https://developer.android.com/reference/tools/gradle-api/4.1/com/android/build/api/dsl/ExternalNativeBuildOptions
                arguments.add("-DANDROID_STL=c++_shared")
                arguments.add("-DANDROID=1")
            }
        }

        ndk {
            if (abiFilters != null) {
                abiFilters.add("x86")
                abiFilters.add("x86_64")
                abiFilters.add("armeabi-v7a")
                abiFilters.add("arm64-v8a")
            } else {
                println("Warning: abiFilters == null")
            }
        }

        ndkVersion = "21.1.6352462"
    }

    buildTypes {
        getByName("debug")
        getByName("release") {
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_11.toString()
        kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    externalNativeBuild {
        cmake {

            path = file("./src/main/cpp/CMakeLists.txt")
            // CMake needs to support at minimum:
            // - FindPython3.cmake module - 3.12.0+
            // - target_link_directories - 3.13.0+
            //
            // If attempt to lower the version for your build, but be aware
            // of the above requirements.
            setVersion("3.13.0+")
        }
    }

    packagingOptions {
        resources.excludes.add("META-INF/*md")
    }

    sourceSets {
        all {
            java.srcDir("src/$name/kotlin")
        }
    }
}

dependencies {
    implementation("androidx.room", "room-runtime", roomVersion)
    implementation(kotlin("reflect"))
    implementation(kotlin("stdlib-jdk8"))

    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.5.2")
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-cbor", serializationVersion)
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-core", serializationVersion)
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-json", serializationVersion)

    kapt("androidx.room", "room-compiler", roomVersion)

    testImplementation("org.junit.jupiter", "junit-jupiter", junitVersion)

    androidTestImplementation("androidx.test", "core", "1.4.0")
    androidTestImplementation("androidx.test.ext", "junit", "1.1.3")
    androidTestImplementation("androidx.test", "runner", "1.4.0")
    androidTestImplementation("com.google.truth", "truth", "1.1.3")
    androidTestImplementation("org.jetbrains.kotlin", "kotlin-test")
    androidTestImplementation("wf.bitcoin", "bitcoin-rpc-client", "1.1.1")
}

tasks.withType<Test> {
    // Tests that do not require external shared library
    useJUnitPlatform()
}

fun Project.getKtlintConfiguration(): Configuration {
    return configurations.findByName("ktlint") ?: configurations.create("ktlint") {
        val dependency = project.dependencies.create("com.pinterest:ktlint:0.42.1")
        dependencies.add(dependency)
    }
}

/**
 * List of files that are linted.
 */
fun lintedFileList(): List<String> {
    return listOf(
        "build.gradle.kts",
        "src/androidTest/**/MultiOptionVoteTests.kt",
        "src/androidTest/**/PayloadContractTests.kt",
        "src/androidTest/**/RingSignatureTest.kt",
        "src/androidTest/**/VoteTestUtil.kt",
        "src/androidTest/**/vote/*.kt",
        "src/main/**/Key.kt",
        "src/main/**/MultiOptionVote.kt",
        "src/main/**/NetworkConstants.kt",
        "src/main/**/PayloadContract.kt",
        "src/main/**/PayloadContractDestination.kt",
        "src/main/**/RingSignature.kt",
        "src/main/**/UtilStringEncoding.kt",
        "src/main/**/vote/*.kt",
        "src/test/**/*.kt"
    )
}

tasks.register("ktlint", JavaExec::class.java) {
    description = "Check Kotlin code style."
    group = "Verification"
    classpath = getKtlintConfiguration()
    main = "com.pinterest.ktlint.Main"
    args = listOf("--android") + lintedFileList()
}
tasks.register("ktlintFormat", JavaExec::class.java) {
    description = "Fix Kotlin code style deviations."
    group = "formatting"
    classpath = getKtlintConfiguration()
    main = "com.pinterest.ktlint.Main"
    args = listOf("-F", "--android") + lintedFileList()
    if (javaVersion.isJava9Compatible) {
        // Fix for InaccessibleObjectException after upgrading from Java 1.8
        // https://github.com/gradle/gradle/issues/1095#issuecomment-270476256
        jvmArgs = mutableListOf("--add-opens", "java.base/java.lang=ALL-UNNAMED")
    }
}

// Publishing
group = "info.bitcoinunlimited"
version = getVersionCode()

publishing {
    publications {
        create<MavenPublication>("libbitcoincash") {
            artifact("$buildDir/outputs/aar/libbitcoincash-release.aar")
        }
    }

    repositories {
        maven {
            url = uri("$buildDir/repos")
        }
    }
}

tasks.map {
    if (it.name.startsWith("publish")) {
        it.dependsOn(tasks.findByName("build")!!)
    }
}