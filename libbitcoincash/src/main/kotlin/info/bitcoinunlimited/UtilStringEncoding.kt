package bitcoinunlimited.libbitcoincash

class UtilStringEncoding {
    companion object {
        @JvmStatic
        fun hexToByteArray(hex: String): ByteArray {
            val HEX_CHARS = "0123456789abcdef"
            val result = ByteArray(hex.length / 2)

            for (i in 0 until hex.length step 2) {
                val firstIndex = HEX_CHARS.indexOf(hex[i])
                val secondIndex = HEX_CHARS.indexOf(hex[i + 1])

                val octet = firstIndex.shl(4).or(secondIndex)
                result.set(i.shr(1), octet.toByte())
            }

            return result
        }

        @JvmStatic
        fun toHexString(array: ByteArray): String {
            return array.joinToString("") { "%02x".format(it) }
        }
    }
}