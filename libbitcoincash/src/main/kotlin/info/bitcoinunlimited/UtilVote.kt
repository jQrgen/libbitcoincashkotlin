package bitcoinunlimited.libbitcoincash

class UtilVote
{
    companion object
    {

        /**
         * Convert a Long value to a ByteArray representing U32Int
         */
        fun longToUInt32ByteArray(value: Long): ByteArray
        {
            val bytes = ByteArray(4)
            bytes[3] = (value and 0xFFFF).toByte()
            bytes[2] = ((value ushr 8) and 0xFFFF).toByte()
            bytes[1] = ((value ushr 16) and 0xFFFF).toByte()
            bytes[0] = ((value ushr 24) and 0xFFFF).toByte()
            return bytes
        }

        /**
         * Sort array of bytearray.
         *
         * Workaround for .sort() throwing "byte[] cannot be cast to java.lang.Comparable".
         */
        fun sortByteArray(array: Array<ByteArray>): Array<ByteArray>
        {
            var asStr: MutableList<String> = mutableListOf()
            for (e in array)
            {
                asStr.add(UtilStringEncoding.toHexString(e))
            }
            asStr.sort()
            var sorted: MutableList<ByteArray> = mutableListOf()
            for (e in asStr)
            {
                sorted.add(UtilStringEncoding.hexToByteArray(e))
            }
            return sorted.toTypedArray()
        }

        /**
         * Create a signature for input at inputIndex in transaction
         */
        fun signInput(tx: BCHtransaction, inputIndex: Long): ByteArray
        {
            val sigHashType = 0x41

            val input = tx.inputs[inputIndex.toInt()]
            val inputAmount = input.spendable.amount
            val priorOutScript = input.spendable.priorOutScript.flatten()
            val txSerialized = tx.BCHserialize(SerializationType.NETWORK).flatten()
            val secret = input.spendable.secret ?: error("input secret missing")

            if (priorOutScript.isEmpty())
            {
                error("Prior output script missing")
            }

            val sig = Wallet.signOneInputUsingSchnorr(
              txSerialized,
              sigHashType,
              inputIndex, inputAmount,
              priorOutScript,
              secret.getSecret()
            )
            if (sig.size != NetworkConstants.SCHNORR_SIGNATURE_SIZE)
            {
                error("Invalid signature size. Expected ${NetworkConstants.SCHNORR_SIGNATURE_SIZE}, was ${sig.size}")
            }

            return sig
        }

        fun hash160Salted(salt: ByteArray, buffer: ByteArray): ByteArray
        {
            return Hash.hash160(salt + buffer)
        }

        /**
         * Get how many bytes it takes to represent given number
         */
        fun varIntSize(number: Int): Int
        {
            if (number <= 0xfc)
            {
                return 1
            }
            if (number <= 0xffff)
            {
                return 3
            }
            if (number <= 0xffffffff)
            {
                return 5
            }
            return 9
        }

        /**
         * Get the size of a transaction with given parameters, excluding input and outputs.
         */
        fun getBaseTxSize(inputCount: Int, outputCount: Int): Int
        {
            return (
              4 /* version */
                + varIntSize(inputCount)
                + varIntSize(outputCount)
                + 4 /* locktime */
              )

        }

        fun byteArrayContains(array: Array<ByteArray>, element: ByteArray): Boolean {
            for (e in array) {
                if (e.contentEquals(element)) return true
            }
            return false
        }
    }
}
