// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash

import kotlinx.serialization.descriptors.PrimitiveKind
import kotlin.system.measureTimeMillis

private val LogIt = GetLog("BU.script")


// Note enum does not work for these script opcodes because I want the opcodes to be the same type as ByteArray so that data and opcodes can be pushed in the same parameter, list, or varargs
// without qualifying the enum with .ordinal
class OP(val v: ByteArray)
{
    companion object
    {
        val FALSE = byteArrayOf(0x0.toByte())
        val TRUE = byteArrayOf(0x51.toByte())

        val NOP = byteArrayOf(0x61.toByte())    // control operations
        val VER = byteArrayOf(0x62.toByte())
        val IF = byteArrayOf(0x63.toByte())
        val NOTIF = byteArrayOf(0x64.toByte())
        val VERIF = byteArrayOf(0x65.toByte())
        val VERNOTIF = byteArrayOf(0x66.toByte())
        val ELSE = byteArrayOf(0x67.toByte())
        val ENDIF = byteArrayOf(0x68.toByte())
        val VERIFY = byteArrayOf(0x69.toByte())
        val RETURN = byteArrayOf(0x6a.toByte())
        val TOALTSTACK = byteArrayOf(0x6b.toByte())     // stack operations
        val FROMALTSTACK = byteArrayOf(0x6c.toByte())
        val DROP2 = byteArrayOf(0x6d.toByte())
        val DUP2 = byteArrayOf(0x6e.toByte())
        val DUP3 = byteArrayOf(0x6f.toByte())

        val OVER2 = byteArrayOf(0x70.toByte())
        val ROT2 = byteArrayOf(0x71.toByte())
        val SWAP2 = byteArrayOf(0x72.toByte())
        val IFDUP = byteArrayOf(0x73.toByte())
        val DEPTH = byteArrayOf(0x74.toByte())
        val DROP = byteArrayOf(0x75.toByte())
        val DUP = byteArrayOf(0x76.toByte())
        val NIP = byteArrayOf(0x77.toByte())
        val OVER = byteArrayOf(0x78.toByte())
        val PICK = byteArrayOf(0x79.toByte())
        val ROLL = byteArrayOf(0x7a.toByte())
        val ROT = byteArrayOf(0x7b.toByte())
        val SWAP = byteArrayOf(0x7c.toByte())
        val TUCK = byteArrayOf(0x7d.toByte())
        val CAT = byteArrayOf(0x7e.toByte())      // string operations
        val SPLIT = byteArrayOf(0x7f.toByte())
        val NUM2BIN = byteArrayOf(0x80.toByte())  // number conversion

        val BIN2NUM = byteArrayOf(0x81.toByte())
        val SIZE = byteArrayOf(0x82.toByte())
        val INVERT = byteArrayOf(0x83.toByte())
        val AND = byteArrayOf(0x84.toByte())
        val OR = byteArrayOf(0x85.toByte())
        val XOR = byteArrayOf(0x86.toByte())
        val EQUAL = byteArrayOf(0x87.toByte())
        val EQUALVERIFY = byteArrayOf(0x88.toByte())
        val RESERVED1 = byteArrayOf(0x89.toByte())
        val RESERVED2 = byteArrayOf(0x8a.toByte())
        val ADD1 = byteArrayOf(0x8b.toByte())
        val SUB1 = byteArrayOf(0x8c.toByte())
        val MUL2 = byteArrayOf(0x8d.toByte())
        val DIV2 = byteArrayOf(0x8e.toByte())
        val NEGATE = byteArrayOf(0x8f.toByte())

        val ABS = byteArrayOf(0x90.toByte())
        val NOT = byteArrayOf(0x91.toByte())
        val NOTEQUAL0 = byteArrayOf(0x92.toByte())
        val ADD = byteArrayOf(0x93.toByte())
        val SUB = byteArrayOf(0x94.toByte())
        val MUL = byteArrayOf(0x95.toByte())
        val DIV = byteArrayOf(0x96.toByte())
        val MOD = byteArrayOf(0x97.toByte())
        val LSHIFT = byteArrayOf(0x98.toByte())
        val RSHIFT = byteArrayOf(0x99.toByte())

        val RIPEMD160 = byteArrayOf(0xa6.toByte())
        val SHA1 = byteArrayOf(0xa7.toByte())
        val SHA256 = byteArrayOf(0xa8.toByte())
        val HASH160 = byteArrayOf(0xa9.toByte())
        val HASH256 = byteArrayOf(0xaa.toByte())
        val CODESEPARATOR = byteArrayOf(0xab.toByte())
        val CHECKSIG = byteArrayOf(0xac.toByte())
        val CHECKSIGVERIFY = byteArrayOf(0xad.toByte())
        val CHECKMULTISIG = byteArrayOf(0xae.toByte())
        val CHECKMULTISIGVERIFY = byteArrayOf(0xaf.toByte())
        val CHECKDATASIG = byteArrayOf(0xba.toByte())
        val CHECKDATASIGVERIFY = byteArrayOf(0xbb.toByte())

        val PUSHDATA1 = byteArrayOf(0x4c.toByte())
        val PUSHDATA2 = byteArrayOf(0x4d.toByte())
        val PUSHDATA4 = byteArrayOf(0x4e.toByte())

        val OP0 = FALSE
        val OP1NEGATE = byteArrayOf(0x4f.toByte())
        val OP1 = TRUE
        val OP2 = byteArrayOf(0x52.toByte())
        val OP3 = byteArrayOf(0x53.toByte())
        val OP4 = byteArrayOf(0x54.toByte())
        val OP5 = byteArrayOf(0x55.toByte())
        val OP6 = byteArrayOf(0x56.toByte())
        val OP7 = byteArrayOf(0x57.toByte())
        val OP8 = byteArrayOf(0x58.toByte())
        val OP9 = byteArrayOf(0x59.toByte())
        val OP10 = byteArrayOf(0x5a.toByte())
        val OP11 = byteArrayOf(0x5b.toByte())
        val OP12 = byteArrayOf(0x5c.toByte())
        val OP13 = byteArrayOf(0x5d.toByte())
        val OP14 = byteArrayOf(0x5e.toByte())
        val OP15 = byteArrayOf(0x5f.toByte())
        val OP16 = byteArrayOf(0x60.toByte())

        // Nextchain
        val PLACE = byteArrayOf(0xe9.toByte())
        val PUSH_TX_STATE = byteArrayOf(0xea.toByte())
        val SETBMD = byteArrayOf(0xeb.toByte())
        val BIN2BIGNUM = byteArrayOf(0xec.toByte())
        val EXEC = byteArrayOf(0xed.toByte())
        val GROUP = byteArrayOf(0xee.toByte())
        val TEMPLATE = byteArrayOf(0xef.toByte())

        // Not real opcodes -- used to match data types in script identification comparisons
        val TMPL_BIGINTEGER = byteArrayOf(0xf0.toByte())
        val TMPL_DATA = byteArrayOf(0xf1.toByte())
        val TMPL_SCRIPT = byteArrayOf(0xf2.toByte())  // replace with any constraint script
        val TMPL_SMALLINTEGER = byteArrayOf(0xfa.toByte())
        val TMPL_PUBKEYS = byteArrayOf(0xfb.toByte())
        val TMPL_PUBKEYHASH = byteArrayOf(0xfd.toByte())
        val TMPL_PUBKEY = byteArrayOf(0xfe.toByte())

        val LAST_DATAPUSH = 0x4b

        fun isPushData0(v: Byte): Int
        {
            if ((v >= 1) && (v <= LAST_DATAPUSH)) return v.toPositiveInt()
            return 0
        }

        /**
         * If the opcode is a "direct numeric push" (opcodes OP1NEGATE -> OP16),
         * return the value OP code represents as ByteArray. Otherwise, null.
         */
        fun getDirectPush(opcode: Byte): ByteArray?
        {
            when (opcode)
            {
                OP0[0] ->
                {
                    return ByteArray(0)
                }
                OP1NEGATE[0],
                OP1[0],
                OP2[0],
                OP3[0],
                OP4[0],
                OP5[0],
                OP6[0],
                OP7[0],
                OP8[0],
                OP9[0],
                OP10[0],
                OP11[0],
                OP12[0],
                OP13[0],
                OP14[0],
                OP15[0],
                OP16[0] ->
                {
                    val value: Int = opcode.toPositiveInt() - (OP1[0].toPositiveInt() - 1)
                    return byteArrayOf(value.toByte())
                }
                else ->
                {
                    return null
                }
            }
        }

        fun push(data: ByteArray): ByteArray
        {
            if (data.size <= LAST_DATAPUSH)
            {
                return byteArrayOf(data.size.toByte()) + data
            }
            if (data.size <= 0xff)
            {
                return PUSHDATA1 + data.size.toByte() + data
            }
            if (data.size <= 0xffff)
            {
                // Size as little endian 16 bits
                val size = ByteArray(2)
                size[0] = (data.size and 0xff).toByte()
                size[1] = (data.size shr 8 and 0xff).toByte()
                return PUSHDATA2 + size + data
            }
            else
                throw UnimplementedException("Pushing more data then stack allows")
            //    return PUSHDATA4 + data.size.toInt() + data
        }

        fun push(num: Int): ByteArray // Pushes a scriptnum
        {
            if (num == 0) return OP0
            if (num <= 16)
            {
                var ret = OP1.clone()
                ret[0] = (ret[0] + (num - 1)).toByte()
                return ret
            }
            throw UnimplementedException("Pushing bigger number")
        }

        // Little endian serialization to 2, 4, or 8 bytes as defined in the Group Tokenization Consensus Specification
        fun groupAmount(value: Long): ByteArray
        {
            if (value < 0xffffL) return push(byteArrayOf((value and 0xff).toByte(), (value shr 8).toByte()))
            if (value < 0xffffffffL) return push(byteArrayOf((value and 0xff).toByte(), (value shr 8).toByte(), (value shr 16).toByte(), (value shr 24).toByte()))
            else return push(byteArrayOf((value and 0xff).toByte(), (value shr 8).toByte(), (value shr 16).toByte(), (value shr 24).toByte(),
              (value shr 32).toByte(), (value shr 40).toByte(), (value shr 48).toByte(), (value shr 56).toByte()))
        }

    }
}


/*
enum class OP(val v:ByteArray)  // ByteArray to support future multibyte instructions
{
    DUP(byteArrayOf(0x76.toByte())),
    EQUAL(byteArrayOf(0x87.toByte())),
    EQUALVERIFY(byteArrayOf(0x88.toByte())),
    HASH160  (byteArrayOf(0xa9.toByte())),
    CHECKSIG (byteArrayOf(0xac.toByte())),
    CHECKSIGVERIFY (byteArrayOf(0xad.toByte())),

    PUSHDATA1 (byteArrayOf(0x4c.toByte())),
    PUSHDATA2 (byteArrayOf(0x4d.toByte())),
    PUSHDATA4 (byteArrayOf(0x4e.toByte())),


    // Not real opcodes -- used to match data types in script identification comparisons
    TMPL_BIGINTEGER(byteArrayOf(0xf0.toByte())),
    TMPL_DATA(byteArrayOf(0xf1.toByte())),
    TMPL_SMALLINTEGER(byteArrayOf(0xfa.toByte())),
    TMPL_PUBKEYS(byteArrayOf(0xfb.toByte())),
    TMPL_PUBKEYHASH(byteArrayOf(0xfd.toByte())),
    TMPL_PUBKEY(byteArrayOf(0xfe.toByte()))
    ;

    operator fun plus(script: BCHscript): BCHscript
    {
        var ret = script
        ret.add(v)
        return ret
    }

    companion object
    {

        fun push(data: ByteArray): ByteArray
        {
            if (data.size == 1)
            {
                if (data[0] < 0x4c.toByte()) return byteArrayOf(data[0])
            }
            if (data.size < 256)
                return PUSHDATA1.v + data
            if (data.size < 0x10000)
                return PUSHDATA2.v + data
            else
                return PUSHDATA4.v + data
        }
    }

}
*/

fun scriptNumFrom(scriptBytes: ByteArray): Long
{
    if (scriptBytes[0] == OP.OP0[0]) return 0
    if ((scriptBytes[0] >= OP.OP1[0]) && (scriptBytes[0] <= OP.OP16[0])) return (scriptBytes[0].toPositiveInt() - (OP.OP1[0].toPositiveInt() - 1)).toLong()
    // TODO implement
    throw UnimplementedException("bigger scriptnum parsing is not implemented")
}

fun scriptDataFrom(scriptBytes: ByteArray): ByteArray?
{
    var i = 0
    val cmd = scriptBytes[i]
    if (cmd >= 1 && cmd <= OP.LAST_DATAPUSH)
    {
        val len = cmd.toInt()
        val slice = scriptBytes.sliceArray(range(i + 1, len))
        return slice
    }
    else if (cmd == OP.PUSHDATA1[0])
    {
        val len = scriptBytes[i + 1].toInt()
        val slice = scriptBytes.sliceArray(range(i + 2, len))
        return slice
    }
    else if (cmd == OP.PUSHDATA2[0])
    {
        assert(false)
    }
    else if (cmd == OP.PUSHDATA4[0])
    {
        assert(false)
    }

    return null
}

@cli(Display.Simple, "Scripts constrain spending an output or satisfy other scripts' constraints")
class BCHscript(val chainSelector: ChainSelector) : BCHserializable()
{
    var data = MutableList<ByteArray>(0, { _ -> ByteArray(0) })

    // Construction
    @cli(Display.User, "Construct a script from a hex string")
    constructor(chainSelector: ChainSelector, script: String) : this(chainSelector)
    {
        data.add(element = script.fromHex())
    }

    @cli(Display.User, "Construct a script from a list of raw bytes")
    constructor(chainSelector: ChainSelector, exactBytes: MutableList<ByteArray>) : this(chainSelector)
    {
        for (b in exactBytes)
        {
            data.add(b)
        }
    }

    @cli(Display.User, "Construct a script from raw bytes")
    constructor(chainSelector: ChainSelector, vararg instructions: ByteArray) : this(chainSelector)
    {
        for (i in instructions)
        {
            data.add(i)
        }
    }

    @cli(Display.User, "Construct a script from individual instructions")
    constructor(chainSelector: ChainSelector, vararg instructions: OP) : this(chainSelector)
    {
        for (i in instructions)
        {
            data.add(i.v)
        }
    }

    /** The single SHA256 hash of this script.  Used by some services as a global pointer identifying the script */
    @cli(Display.User, "The single SHA256 hash of this script.  Used by some services as a global pointer identifying the script")
    fun scriptHash(): Hash256
    {
        return Hash256(Hash.sha256(BCHserialize(SerializationType.SCRIPTHASH).flatten()))
    }

    @cli(Display.User, "The RIPEMD160 of the SHA256 hash of this script.  Used by P2SH scripts")
    fun scriptHash160(): ByteArray
    {
        return Hash.hash160(BCHserialize(SerializationType.SCRIPTHASH).flatten())
    }


    @cli(Display.User, "Are 2 scripts the same?")
    fun contentEquals(other: BCHscript): Boolean
    {
        val a = flatten()
        val b = other.flatten()
        return a.contentEquals(b)
    }

    override fun equals(other: Any?): Boolean
    {
        if (other !is BCHscript) return false
        return this.contentEquals(other)
    }

    @cli(Display.User, "The length of this script in bytes")
    val size: Int
        get() = this.flatten().size

    @cli(Display.User, "Make a copy of this script")
    fun copy(): BCHscript
    {
        return BCHscript(chainSelector, this.flatten().clone())
    }

    @cli(Display.User, "Append raw script bytes to this script, returning a new script.  Note! Use  '+ OP.push(...)' to push data into a script")
    operator fun plus(rawscript: ByteArray): BCHscript
    {
        var ret = this.copy()
        ret.add(rawscript)
        return ret
    }

    @cli(Display.User, "concatenate a script to the end of this one, returning the joined script")
    operator fun plus(script: BCHscript): BCHscript
    {
        var ret = this.copy()
        ret.add(script.flatten())
        return ret
    }

    // Composition
    @cli(Display.User, "Append a new instruction")
    fun add(cmd: Byte): BCHscript
    {
        data.add(ByteArray(1, { _ -> cmd }))
        return this
    }

    @cli(Display.User, "Append raw script bytes to this script.  Note! Use add(OP.push(...)) to push data into a script")
    fun add(cmds: ByteArray): BCHscript
    {
        data.add(cmds)
        return this
    }

    // Converts the internal representation to a single ByteArray
    @cli(Display.User, "Converts the internal representation to a single ByteArray")
    fun flatten(): ByteArray
    {
        if (data.size == 0) return byteArrayOf()
        if (data.size > 1)
            data = MutableList<ByteArray>(1, { _ -> data.join() })
        return data[0]
    }

    // Parse is the opposite of flatten in the sense that each element in data will be 1 instruction
    fun parsed(): MutableList<ByteArray>
    {
        val scriptBytes = flatten()
        var i = 0
        val parsed = MutableList<ByteArray>(0, { _ -> ByteArray(0) })
        while (i < scriptBytes.size)
        {
            val cmd = scriptBytes[i]
            if (cmd >= 1 && cmd <= OP.LAST_DATAPUSH)
            {
                val len = cmd.toInt()
                val slice = scriptBytes.sliceArray(range(i, len + 1))
                parsed.add(slice)
                i = i + len + 1
            }
            else if (cmd == OP.PUSHDATA1[0])
            {
                val j = i + 1
                val len = scriptBytes[j].toInt()
                val slice = scriptBytes.sliceArray(range(i, len + 2))
                parsed.add(slice)
                i = i + len + 2
            }
            else if (cmd == OP.PUSHDATA2[0])
            {
                assert(false)
            }
            else if (cmd == OP.PUSHDATA4[0])
            {
                assert(false)
            }
            else
            {
                parsed.add(byteArrayOf(cmd))
                i++
            }

        }
        return parsed
    }

    // Serialization
    @cli(Display.User, "Create a script from serialized data")
    constructor(chainSelector: ChainSelector, buf: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    @cli(Display.User, "Overwrite this script with serialized data")
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        data.clear()
        var len = stream.decompact()
        data.add(stream.debytes(len))
        return stream
    }

    @cli(Display.User, "Serialize this script")
    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        var alldata: ByteArray = data.join()
        if (format == SerializationType.SCRIPTHASH)
        {
            return BCHserialized(alldata, format)
        }


        var ret = BCHserialized.compact(alldata.size.toLong(), format) + BCHserialized(alldata, format)
        return ret
    }

    @cli(Display.User, "Convert to serialized byte array (suitable for network, execution or hashing)")
    fun toByteArray(): ByteArray
    {
        return BCHserialize(SerializationType.SCRIPTHASH).flatten()
    }


    data class MatchResult(val type: PayAddressType, val params: MutableList<ByteArray>, val grouped: Boolean = false)
    {
    }

    @cli(Display.User, "Parse common script types (P2PKH and P2SH) and return useful data")
    fun match(): MatchResult?
    {
        matches(P2PKHtemplate)?.run { return MatchResult(PayAddressType.P2PKH, this) }
        matches(P2SHtemplate)?.run { return MatchResult(PayAddressType.P2SH, this) }
        matches(GroupP2PKHtemplate)?.run { return MatchResult(PayAddressType.P2PKH, this, true) }
        matches(GroupP2SHtemplate)?.run { return MatchResult(PayAddressType.P2SH, this, true) }
        return null
    }

    fun replace(mapper: (ByteArray) -> BCHscript): BCHscript
    {
        val script = flatten()
        var ret = BCHscript(chainSelector)

        var i = 0 // current location in the script
        while (i < script.size)  // TODO extract the script looper
        {
            if (script[i] == OP.TMPL_PUBKEYHASH[0])
            {
                ret += mapper(OP.TMPL_PUBKEYHASH)
                i++

            }
            else if (script[i] == OP.TMPL_SCRIPT[0])
            {
                ret += mapper(OP.TMPL_SCRIPT)
                i++
            }
            // TODO the other template items
            else if (OP.isPushData0(script[i]) > 0)
            {
                val datalen = script[i]
                i++
                ret.add(OP.push(script.sliceArray(i until i + datalen)))
                i += datalen
            }
            else if (script[i] == OP.PUSHDATA1[0])
            {
                i++
                val datalen = script[i]
                i++
                ret.add(OP.push(script.sliceArray(i until i + datalen)))
                i += datalen
            }
            else if (script[i] == OP.PUSHDATA2[0])  // TODO
            {
                throw UnimplementedException("pushdata 2")
            }
            else if (script[i] == OP.PUSHDATA4[0])  // TODO
            {
                throw UnimplementedException("pushdata 4")
            }
            else  // Handle any other opcode
            {
                ret.add(script[i])
                i++
            }
        }
        return ret
    }

    // Returns true if this script has TMPL parameters in it (its not a valid executable script yet).
    fun parameterized(): Boolean
    {
        val script = flatten()
        var i = 0 // current location in the script
        while (i < script.size)  // TODO extract the script looper
        {
            if (script[i] == OP.TMPL_PUBKEYHASH[0])
            {
                return true
            }
            else if (script[i] == OP.TMPL_SCRIPT[0])
            {
                return true
            }
            // TODO the other template items
            else if (OP.isPushData0(script[i]) > 0)
            {
                val datalen = script[i]
                i++
                i += datalen
            }
            else if (script[i] == OP.PUSHDATA1[0])
            {
                i++
                val datalen = script[i]
                i++
                i += datalen
            }
            else if (script[i] == OP.PUSHDATA2[0])  // TODO
            {
                throw UnimplementedException("pushdata 2")
            }
            else if (script[i] == OP.PUSHDATA4[0])  // TODO
            {
                throw UnimplementedException("pushdata 4")
            }
            else  // Handle any other opcode
            {
                i++
            }
        }
        return false
    }

    @cli(Display.Dev, "If this is a P2SH satisfier script, get its redeem script.\n" +
      "Warning, this just returns the last script data push as a script, so if this is NOT a P2SH satisfier script, you will get weird results.")
    fun getRedeemFromSatisfier(): BCHscript
    {
        val satisfier = parsed()
        val redeemRaw = scriptDataFrom(satisfier[satisfier.size - 1]) ?: byteArrayOf()
        val redeemScript = BCHscript(chainSelector, redeemRaw)
        return redeemScript
    }

    @cli(Display.Dev, "Attempt to match this script (or just the beginning of it) to a template")
    fun matches(template: BCHscript, prefix: Boolean = false): MutableList<ByteArray>?
    {
        val script = flatten()
        val tmpl = template.flatten()
        var ret = mutableListOf<ByteArray>()

        var ti = 0 // current location in the template
        var i = 0 // current location in the script
        while ((i < script.size) && (ti < tmpl.size))
        {
            if (tmpl[ti] == script[i])
            {
                i += 1; ti += 1; continue
            }
            if (tmpl[ti] == OP.TMPL_PUBKEYHASH[0])
            {
                if (OP.isPushData0(script[i]) > 0)  // Handle the direct push case
                {
                    val datalen = script[i]
                    if (datalen != 20.toByte()) return null   // pubkeyhash must be 20 bytes
                    i += 1
                    ret.add(script.sliceArray(IntRange(i, i + 19)))
                    i += 20
                    ti += 1
                    continue
                }

                if (script[i] == OP.PUSHDATA1[0]) // Handle the pushdata1 case
                {
                    i += 1
                    val datalen = script[i]
                    if (datalen != 20.toByte()) return null   // pubkeyhash must be 20 bytes
                    i += 1
                    ret.add(script.sliceArray(IntRange(i, i + 19)))
                    i += 20
                    ti += 1
                    continue
                }

                // TODO the other pushdata operations
                //    PUSHDATA2
                //    PUSHDATA4
            }
            if (tmpl[ti] == OP.TMPL_DATA[0])
            {
                if (OP.isPushData0(script[i]) > 0)  // Handle the direct push case
                {
                    val datalen = script[i]
                    i += 1
                    ret.add(script.sliceArray(i until i + datalen))
                    i += datalen
                    ti += 1
                    continue
                }

                if (script[i] == OP.PUSHDATA1[0]) // Handle the pushdata1 case
                {
                    i += 1
                    val datalen = script[i]
                    i += 1
                    ret.add(script.sliceArray(i until i + datalen))
                    i += datalen
                    ti += 1
                    continue
                }
                // TODO the other pushdata operations
                //    PUSHDATA2
                //    PUSHDATA4
            }

            // TODO the other template items
            return null
        }

        if (prefix && (ti == tmpl.size)) return ret  // if looking for a prefix, just check that the template ended
        if ((i == script.size) && (ti == tmpl.size)) return ret  // both script and template ended at the same moment, template matches
        return null
    }

    // Analysis
    @cli(Display.User, "Parse common script types (P2PKH, P2SH, and grouped variants) and return the address or null if the script is not standard")
    val address: PayAddress? // nullable because its possible that I can't understand this script or that it is an anyone-can-pay (has no constraint)
        get()
        {
            matches(P2PKHtemplate)?.run {
                return PayAddress(chainSelector, PayAddressType.P2PKH, this[0])
            }

            matches(P2SHtemplate)?.run {
                return PayAddress(chainSelector, PayAddressType.P2SH, this[0])
            }
            matches(GroupP2PKHtemplate)?.run {
                LogIt.info("matched gp2pkh: " + this.map { it.toHex() }.joinToString(" "))
                return PayAddress(chainSelector, PayAddressType.P2PKH, this[2])
            }
            matches(GroupP2SHtemplate)?.run {
                return PayAddress(chainSelector, PayAddressType.P2SH, this[2])
            }
            return null
        }

    // turn a byte array (from the script stack) into a group amount
    @kotlin.ExperimentalUnsignedTypes
    fun decodeGroupAmount(ser: ByteArray): Long?
    {
        if (!((ser.size == 2) || (ser.size == 4) || (ser.size == 8))) return null  // amount size is incorrect
        if (ser.size == 2)
        {
            return (ser[0].toULong() + (ser[1].toULong() shl 8)).toLong()
        }
        if (ser.size == 4)
        {
            return (ser[0].toULong() + (ser[1].toULong() shl 8) + (ser[2].toULong() shl 16) + (ser[3].toULong() shl 24)).toLong()
        }
        if (ser.size == 8)
        {
            return (ser[0].toULong() + (ser[1].toULong() shl 8) + (ser[2].toULong() shl 16) + (ser[3].toULong() shl 24) +
              (ser[4].toULong() shl 32) + (ser[5].toULong() shl 40) + (ser[6].toULong() shl 48) + (ser[7].toULong() shl 56)
              ).toLong()
        }
        return null
    }

    @cli(Display.User, "Return the Group annotation associated with this script, or null if ungrouped")
    @kotlin.ExperimentalUnsignedTypes
    fun groupInfo(nativeAmount: Long): GroupInfo?
    {
        val gd = matches(GroupPrefixTemplate, prefix = true) ?: return null  // match failed
        assert(gd.size == 2)  // or template is not what I expected it to be
        val gid = gd[0]
        if (gid.size < GroupId.GROUP_ID_MIN_SIZE) return null  // TODO return something indicating illegal script?
        if (gid.size > GroupId.GROUP_ID_MAX_SIZE) return null
        var amt = decodeGroupAmount(gd[1]) ?: return null
        var authority = 0.toULong()
        val groupId = GroupId(gid)
        if (amt < 0)   // Handle authority flags
        {
            authority = amt.toULong()
            amt = 0
        }
        else
        {
            if (groupId.isFenced())
            {
                if (amt != 0L) return null // return something indicating illegal script?
                amt = nativeAmount
            }
        }
        return GroupInfo(GroupId(gid), amt, authority)
    }

    @cli(Display.User, "Return the P2SH constraint (output) script corresponding to this redeem script")
    fun P2SHconstraint(): BCHscript
    {
        return BCHscript(chainSelector, OP.HASH160, OP.push(scriptHash160()), OP.EQUAL)
    }

    @cli(Display.User, "Return the P2SH part of the satisfier (input) script corresponding to this redeem script.  You must subsequently append a script that actually satisfies the redeem script. ")
    fun P2SHsatisfier(): BCHscript
    {
        return BCHscript(chainSelector, OP.push(toByteArray()))
    }

    @cli(Display.Simple, "Convert to hex notation")
    fun toHex(): String
    {
        return flatten().toHex()
    }

    @cli(Display.User, ("Take a copy of the first pushed stack item as it "
      + "would appear on the stack, returning the copy and the range copied "
      + "from. Throws if first OP is not a PUSH operation."))
    fun copyFront(): Pair<ByteArray, IntRange>
    {
        return copyStackElementAt(0)
    }

    @cli(Display.User, ("Take a copy of stack element at offset as it would "
      + "appear on the stack, returning the copy and the range copied from. "
      + "Offset MUST point to a PUSH operation."))
    fun copyStackElementAt(offset: Int): Pair<ByteArray, IntRange>
    {
        val script = flatten()
        var i = offset
        val pushOP = script.getOrNull(i++) ?: throw IllegalArgumentException("Offset $offset is out of bounds. Size of script is ${script.size}.")
        OP.getDirectPush(pushOP)?.let {
            return Pair(it, IntRange(offset, offset + it.size))
        }
        if (OP.isPushData0(pushOP) != 0)
        {
            val range = IntRange(i, i + pushOP.toPositiveInt() - 1)
            val stackItem = script.sliceArray(range)
            return Pair(stackItem, range)
        }
        if (pushOP == OP.PUSHDATA1[0])
        {
            val stackItemLen = script[i++].toPositiveInt()
            val range = IntRange(i, i + stackItemLen - 1)
            val stackItem = script.sliceArray(range)
            return Pair(stackItem, range)
        }
        if (pushOP == OP.PUSHDATA2[0])
        {
            val stackItemLen: ByteArray = script.copyOfRange(i++, ++i)

            val stackItemLenBE = (
              (stackItemLen[1].toUint() shl 8) or stackItemLen[0].toUint())

            val range = IntRange(i, i + stackItemLenBE.toInt() - 1)
            val stackItem = script.sliceArray(range)
            return Pair(stackItem, range)
        }
        if (pushOP == OP.PUSHDATA4[0])
        {
            TODO("PUSHDATA4")
        }
        throw IllegalArgumentException("Element at offset ${offset} is not a PUSH operation (found ${pushOP.toPositiveInt()})")
    }

    override fun toString(): String = "BCHscript(\"" + toHex() + "\")"

    // Execution
    companion object
    {
        // Right now, the chain is ignored in these templates since multiple blockchains support the same script templates
        val P2PKHtemplate = BCHscript(ChainSelector.BCHMAINNET, OP.DUP, OP.HASH160, OP.TMPL_PUBKEYHASH, OP.EQUALVERIFY, OP.CHECKSIG)
        val P2SHtemplate = BCHscript(ChainSelector.BCHMAINNET, OP.HASH160, OP.TMPL_PUBKEYHASH, OP.EQUAL)

        // These do not match that the group data is the correct size so more checking is needed
        val GroupPrefixTemplate = BCHscript(ChainSelector.BCHMAINNET, OP.TMPL_DATA, OP.TMPL_DATA, OP.GROUP)
        val GroupP2PKHtemplate = GroupPrefixTemplate + P2PKHtemplate
        val GroupP2SHtemplate = GroupPrefixTemplate + P2SHtemplate

        init
        {
            GroupP2PKHtemplate.flatten()
            GroupP2SHtemplate.flatten()
        }

        @cli(Display.Simple, "Create a pay-to-public-key-hash constraint script")
        fun p2pkh(rawAddr: ByteArray, chainSelector: ChainSelector = ChainSelector.BCHMAINNET): BCHscript
        {
            assert(rawAddr.size == 20)  // Otherwise this p2pkh is unspendable
            return BCHscript(chainSelector, OP.DUP, OP.HASH160, OP.push(rawAddr), OP.EQUALVERIFY, OP.CHECKSIG)
        }

        @cli(Display.Simple, "Create a pay-to-script-hash constraint script")
        fun p2sh(rawAddr: ByteArray, chainSelector: ChainSelector = ChainSelector.BCHMAINNET): BCHscript
        {
            assert(rawAddr.size == 20)  // Otherwise this p2sh is unspendable
            return BCHscript(chainSelector, OP.HASH160, OP.push(rawAddr), OP.EQUAL)
        }

        @cli(Display.Simple, "Create a grouped script prefix")
        fun grouped(grpId: GroupId, tokenAmt: Long, chainSelector: ChainSelector = ChainSelector.BCHMAINNET): BCHscript
        {
            assert(grpId.data.size >= 32)  // GroupIds must be 32 bytes or more
            return BCHscript(chainSelector, OP.push(grpId.data), OP.groupAmount(tokenAmt), OP.GROUP)
        }

        @cli(Display.Simple, "Create a grouped pay-to-public-key-hash constraint script")
        fun gp2pkh(grpId: ByteArray, tokenAmt: Long, rawAddr: ByteArray, chainSelector: ChainSelector = ChainSelector.BCHMAINNET): BCHscript
        {
            assert(grpId.size >= 32)  // GroupIds must be 32 bytes or more
            assert(rawAddr.size == 20)  // Otherwise this p2pkh is unspendable
            return BCHscript(chainSelector, OP.push(grpId), OP.groupAmount(tokenAmt), OP.GROUP, OP.DUP, OP.HASH160, OP.push(rawAddr), OP.EQUALVERIFY, OP.CHECKSIG)
        }

        fun gp2pkh(grpId: GroupId, tokenAmt: Long, rawAddr: ByteArray, chainSelector: ChainSelector = ChainSelector.BCHMAINNET): BCHscript = gp2pkh(grpId.data, tokenAmt, rawAddr, chainSelector)

        @cli(Display.Simple, "Create a grouped pay-to-script-hash constraint script")
        fun gp2sh(grpId: ByteArray, tokenAmt: Long, rawAddr: ByteArray, chainSelector: ChainSelector = ChainSelector.BCHMAINNET): BCHscript
        {
            assert(grpId.size >= 32)  // GroupIds must be 32 bytes or more
            assert(rawAddr.size == 20)  // Otherwise this p2sh is unspendable
            return BCHscript(chainSelector, OP.push(grpId), OP.groupAmount(tokenAmt), OP.GROUP, OP.HASH160, OP.push(rawAddr), OP.EQUAL)
        }

        fun gp2sh(grpId: GroupId, tokenAmt: Long, rawAddr: ByteArray, chainSelector: ChainSelector = ChainSelector.BCHMAINNET): BCHscript = gp2sh(grpId.data, tokenAmt, rawAddr, chainSelector)
    }
}

