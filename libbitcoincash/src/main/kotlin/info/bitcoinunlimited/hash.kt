// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash

class Hash
{
    companion object
    {
        @JvmStatic
        external fun hash256(data: ByteArray): ByteArray

        @JvmStatic
        external fun sha256(data: ByteArray): ByteArray

        @JvmStatic
        external fun hash160(data: ByteArray): ByteArray
    }
}