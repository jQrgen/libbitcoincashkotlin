// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash

import java.math.BigInteger

private val LogIt = GetLog("BU.block")


/** Header of a block for bitcoin-family cryptocurrencies */
@cli(Display.Simple, "Block header")
open class BlockHeader() : BCHserializable()
{
    var hexHash = String()
    var hashData: Hash256 = Hash256()

    @cli(Display.Simple, "block version")
    var version: Int = 0

    @cli(Display.Simple, "previous block hash")
    var hashPrevBlock = Hash256()

    @cli(Display.Simple, "merkle root hash")
    var hashMerkleRoot = Hash256()

    @cli(Display.Simple, "block timestamp")
    var time: Long = 0L

    @cli(Display.Simple, "difficulty in 'bits' representation")
    var diffBits: Long = 0L // nBits

    @cli(Display.Simple, "nonce")
    var nonce: Long = 0L

    // This data is not part of the consensus header
    @cli(Display.Simple, "number of tx in this block")
    var numTx: Long = -1

    @cli(Display.Simple, "block size in bytes")
    var blockSize: Long = -1

    @cli(Display.Simple, "cumulative work")
    var cumulativeWork: BigInteger = 0.toBigInteger()

    @cli(Display.Simple, "block height")
    var height: Long = -1

    @cli(Display.Simple, "ancestor")
    var hashAncestor = Hash256() // For more rapid rewind, an ancestor is stored when this block is saved

    companion object
    {
        @JvmStatic
        fun fromHex(hex: String, serializationType: SerializationType = SerializationType.UNKNOWN): BlockHeader
        {
            val bytes = hex.fromHex()
            var header = BlockHeader()
            header.deserHeaderFields(BCHserialized(bytes, serializationType))
            return header
        }
    }

    fun work(): Long
    {
        return diffBits
    }


    /** Force recalculation of hash. To access the hash just use #hash */
    fun calcHash(): Hash256
    {
        // call header serialize explicitly in case derived class has overridden BCHserialize
        var ser = BCHserializeHeader(SerializationType.HASH)
        ser.flatten()
        val h = Hash256(bitcoinunlimited.libbitcoincash.Hash.hash256(ser.flatten()))
        hashData = h
        return h
    }

    /** If you change block data, call this function to force lazy recalculation of the hash */
    fun invalidateHash()
    {
        hashData = Hash256() // null
    }

    /** Return this block's hash.  Uses a cached value if one exists.  If you change the block be sure to call calcHash() or invalidateHash() to update this value either greedily or lazily respectively */
    @cli(Display.Simple, "block hash")
    var hash: Hash256
        get()
        {
            var temp = hashData
            if (temp == Hash256()) // ((temp == null) || (temp == Hash256()))
            {
                return calcHash()
            }
            return temp
        }
        set(value)
        {
            hashData = value
        }

    /** assignment constructor */
    constructor(_hash: Hash256, _version: Int, _time: Long, _diffBits: Long, _nonce: Long, _hashMerkleRoot: Hash256, _hashPrevBlock: Hash256) : this()
    {
        hash = _hash
        hexHash = hash.toHex()

        version = _version
        time = _time
        diffBits = _diffBits
        nonce = _nonce
        hashMerkleRoot = _hashMerkleRoot
        hashPrevBlock = _hashPrevBlock

    }

    constructor(stream: BCHserialized) : this() //!< deserializing constructor
    {
        BCHdeserialize(stream)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized = BCHserializeHeader(format) //!< Serializer

    fun BCHserializeHeader(format: SerializationType): BCHserialized
    {
        var serialized = BCHserialized(format) + BCHserialized.int32(version) + hashPrevBlock + hashMerkleRoot + BCHserialized.uint32(time) +
          BCHserialized.uint32(diffBits) + BCHserialized.uint32(nonce)

        if (format != SerializationType.HASH)
            BCHserialized.compact(numTx)

        if (format == SerializationType.DISK)
        {
            serialized.add(BCHserialized.compact(blockSize))
            serialized.add(hashAncestor)
            // serialized.add(BCHserialized.compact(cumulativeWork))
        }
        return serialized
    }

    /** Deserialize just the header fields (used when this header is included in other objects/messages */
    fun deserHeaderFields(stream: BCHserialized): BCHserialized
    {
        // header fields
        version = stream.deint32()
        hashPrevBlock.BCHdeserialize(stream)
        hashMerkleRoot.BCHdeserialize(stream)
        time = stream.deuint32()
        diffBits = stream.deuint32()
        nonce = stream.deuint32()
        return stream
    }

    /** Deserializer
    Note that this includes the number of tx in NETWORK serialization as a legacy of the P2P network protocol.
    In DISK serialization, the block size in bytes is included.

    When the header is part of the block, the numTx is not included as an independent field so use deserHeaderFields() API or HASH SerializationType
     */
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        deserHeaderFields(stream)

        if (stream.format == SerializationType.NETWORK) numTx = stream.decompact()  // P2P Network only

        if (stream.format == SerializationType.DISK)
        {
            blockSize = stream.decompact()
            hashAncestor.BCHdeserialize(stream)
            // cumulativeWork = stream.decompact()
        }
        return stream
    }
}

/** A block that contains a subset of the total number of transactions, and includes a merkle proof that the provided transactions are part of the block */
class MerkleBlock(val chainSelector: ChainSelector) : BlockHeader()
{
    // Hashes of either transactions or merkle sub-trees
    //var hashes: List<Hash256>? = null
    // Bit data that describes how the hashes are used to form a merkle proof.
    //var merkleProofPath: ByteArray? = null

    //* the hashes of the transactions provided by this merkle block
    val txHashes: MutableSet<Hash256> = mutableSetOf()

    //* the actual transactions provided by this merkle block
    val txes: MutableList<BCHtransaction> = mutableListOf()


    val complete: Boolean  //*< return true if all needed transactions are available in this merkle block
        @Synchronized
        get()
        {
            if (txHashes.size == 0)
            {
                //LogIt.finer("Merkle block provided ${txes.size} of ${numTx} total transactions")
                return true
            }  // If we are out of txHashes, txes should be full

            assert(txes.size < numTx) // otherwise it should not be full
            return false
        }

    /** Call to offer a transaction that this merkleblock might contain.
     * @return true if this merkleblock consumed this transaction */
    @Synchronized
    fun txArrived(tx: BCHtransaction): Boolean
    {
        if (txHashes.contains(tx.hash))
        {
            txes.add(tx)
            txHashes.remove(tx.hash)
            //LogIt.finer(sourceLoc() + ": Merkle block ${hash.toHex()} tx arrived ${tx.hash.toHex()}.  ${txHashes.size} left to find (of ${numTx}).")
            return true
        }
        return false
    }

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector) //!< stream constructor
    {
        BCHdeserialize(stream)
    }

    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        deserHeaderFields(stream)
        numTx = stream.deuint32()
        val h: Array<ByteArray> = stream.delist { Hash256(it).hash }.toTypedArray()  // The merkle block proof data, containing either inner hashes or tx hashes
        val mpp = stream.deByteArray()

        val result = Extract(numTx.toInt(), mpp, h)  // Validate the merkle block proof, extracting the transaction hashes
        if ((result == null) || (result.size == 0) || (Hash256(result[0]) != hashMerkleRoot))
        {
            throw DeserializationException("Merkle block inconsistent", "network error")
        }

        //val logstr = StringBuilder()
        //logstr.append("Deserialize merkle block: ${hash.toHex()}, requires ${result.size-1} (${numTx}) TX: ")
        // Fill with the hashes we need.  result[0] is the merkle root
        for (i in 1 until result.size)
        {
            txHashes.add(Hash256(result[i]))
            //logstr.append(Hash256(result[i]).toHex())
            //logstr.append(" ")
        }

        //LogIt.finer(sourceLoc() + " " + logstr.toString())
        return stream
    }

    companion object
    {
        @JvmStatic
        external fun Extract(numTxes: Int, merkleProofPath: ByteArray, hashes: Array<ByteArray>): Array<ByteArray>?
    }
}

class BCHblock(val chainSelector: ChainSelector) : BlockHeader()
{
    var txes: MutableList<BCHtransaction> = mutableListOf()

    constructor(chainSelector: ChainSelector, stream: BCHserialized) : this(chainSelector)
    {
        BCHdeserialize(stream)
    }


    override fun BCHserialize(format: SerializationType): BCHserialized //!< Serializer
    {
        // serialization for hash calc is only the header, not the tx.  merkle root in header captures tx entropy
        val data = if (format == SerializationType.HASH)
            super.BCHserialize(format)
        else super.BCHserialize(format) + txes
        return data
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized //!< Deserializer
    {
        deserHeaderFields(stream)
        txes = stream.delist { BCHtransaction(chainSelector, it) }
        numTx = txes.size.toLong()
        return stream
    }
}
