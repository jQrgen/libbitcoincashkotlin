package info.bitcoinunlimited.vote

import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Initialize
import bitcoinunlimited.libbitcoincash.PayAddress
import bitcoinunlimited.libbitcoincash.UtilStringEncoding
import bitcoinunlimited.libbitcoincash.vote.MultiOptionVote
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test

class MultiOptionVoteTests {
    @Before
    fun loadlib() {
        System.loadLibrary("bitcoincashandroid")
        Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
    }

    companion object {
        val vote = MultiOptionVote(
            "4f06221a3d48da3a2965d665ee5d8b0441cfb57b65f6b7819620e391b49fc66c".toByteArray(),
            "Test MOV #2",
            732337, 736657,
            arrayOf("Accept", "Reject"),
            arrayOf(
                PayAddress("bitcoincash:qqth3xv8z6aysxedsmwcspgkrwt7lm9whyr5fmawvc").data,
                PayAddress("bitcoincash:qpxt75avxax8uakv47wg46rkp6ay72pnkqq5z2t325").data,
                PayAddress("bitcoincash:qqs37r80l0enh2lykdq4cxdtz57y0amh9ss68thdye").data,
            )
        )
    }

    @Test
    fun testParticipantsMerkleRootHash() {
        assertThat(UtilStringEncoding.toHexString(vote.participantsMerkleRootHash())).isEqualTo("c3bd84f68c6742fb387787f2ca8993e714f3c7f09167e9497b8b473822b355dd")
    }

    @Test
    fun testElectionID() {
        assertThat(UtilStringEncoding.toHexString(vote.electionId())).isEqualTo("c705fde5fc5bb4410f3dd6a9b78b7e4b21312638")
    }

    @Test
    fun testRedeemScript() {
        val hex = UtilStringEncoding.toHexString(
            vote.redeemScript(ChainSelector.BCHMAINNET, vote.participants[0]).toByteArray()
        )
        assertThat(hex).isEqualTo("78ad76a9141778998716ba481b2d86dd8805161b97efecaeb988527914c705fde5fc5bb4410f3dd6a9b78b7e4b21312638887b7b7e7cbb7491")
    }

    @Test
    fun testVoterContractAddresses() {
        assertThat(vote.voterContractAddress(ChainSelector.BCHMAINNET, vote.participants[0]).toString())
            .isEqualTo("bitcoincash:ppj779s6hqwagmly046ce67xmcv963wf75sxkhgv29")
    }
}