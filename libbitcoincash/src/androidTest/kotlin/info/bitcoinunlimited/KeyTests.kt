package info.bitcoinunlimited

import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Key
import bitcoinunlimited.libbitcoincash.Initialize
import bitcoinunlimited.libbitcoincash.UtilStringEncoding
import com.google.common.truth.Truth
import org.junit.Test

class KeyTests
{
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }

    @Test
    fun decodeWIF() {
        val secretWIF = "L3Hq7a8FEQwJkW1M2GNKDW28546Vp5miewcCzSqUD9kCAXrJdS3g"
        val secretDecoded = Key.decodePrivateKey(ChainSelector.BCHMAINNET.v, secretWIF)
        val secretHex = UtilStringEncoding.toHexString(secretDecoded)
        Truth.assertThat(secretHex).isEqualTo("b524c28b61c9b2c49b2c7dd4c2d75887abb78768c054bd7c01af4029f6c0d117")
    }
}