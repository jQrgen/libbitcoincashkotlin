package info.bitcoinunlimited.libbitcoincash

import bitcoinunlimited.libbitcoincash.*
import kotlinx.serialization.Serializable
import org.junit.Test
import org.junit.Before
import java.lang.Thread.sleep

val SimulationHostIP = "10.0.2.2"
// The IP address of the host machine: Android sets up a fake network with the host hardcoded to this IP
val EMULATOR_HOST_IP = SimulationHostIP //"192.168.1.100" //"10.0.2.2"

@Serializable
data class BannerReply(val result: String)

class ElectrumProtocolTests
{
     companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }

    @Before
    fun beforeMethod() {
        val cnxn = try
        {
            ElectrumClient(ChainSelector.BCHREGTEST,
                EMULATOR_HOST_IP,
                DEFAULT_TCP_ELECTRUM_PORT_REGTEST,
                "Electrum@${EMULATOR_HOST_IP}:${DEFAULT_TCP_ELECTRUM_PORT_REGTEST}")
        } catch (e: java.net.ConnectException) {
          LogIt.warning("Skipping Electrum tests: ${e}")
          null
        }

        cnxn?.let {
            it.close()
        }
        org.junit.Assume.assumeTrue(cnxn != null)
    }

    @Test
    fun testelectrumclient()
    {
        LogIt.info("This test requires an electrum cash server running on regtest at ${EMULATOR_HOST_IP}:${DEFAULT_TCP_ELECTRUM_PORT_REGTEST}")
        val cnxn = try
        {
            ElectrumClient(ChainSelector.BCHREGTEST,
                EMULATOR_HOST_IP,
                DEFAULT_TCP_ELECTRUM_PORT_REGTEST,
                "Electrum@${EMULATOR_HOST_IP}:${DEFAULT_TCP_ELECTRUM_PORT_REGTEST}")
        }
        catch(e: Exception)
        {
            LogIt.warning("Exception: " + e.toString())
            LogIt.warning(e.stackTraceToString())
            throw e
        }
        cnxn.start()

        val ret = cnxn.call("server.version", listOf("4.0.1", "1.4"), 1000)
        if (ret!=null) LogIt.info(sourceLoc() + ": Server Version returned: " + ret)

        val version = cnxn.version()
        LogIt.info(sourceLoc() + ": Version API call returned: " + version.first + " " + version.second)

        // TODO enable when electrscash is updated
        val features = cnxn.features()
        LogIt.info(sourceLoc() + ": genesis block hash:" + features.genesis_hash)
        check("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206" == features.genesis_hash)
        check("sha256" == features.hash_function)
        check(features.server_version.contains("ElectrsCash"))  // Clearly this may fail if you connect a different server to this regression test

        val ret2 = cnxn.call("blockchain.block.header", listOf(100, 102), 1000)
        LogIt.info(ret2)

        try {
            cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d00", 5000)  // doesn't exist
            assert(false)
        }
        catch(e: ElectrumIncorrectRequest)
        {
            if (e.message!!.contains("still syncing"))
            {
                LogIt.warning("This test is more effective if you generate 101 blocks on regtest")
            }
            else assert(e.message!!.contains("not indexed tx"))
        }

        try
        {
            cnxn.getTx("zz5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d", 5000) // bad hash
            assert(false)
        }
        catch(e: ElectrumIncorrectRequest)
        {
        }

        try
        {
            cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d", 5000) // bad hash (short)
            assert(false)
        }
        catch(e: ElectrumIncorrectRequest)
        {
        }

        try
        {
            cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d".repeat(10), 1000) // bad hash (large)
            assert(false)
        }
        catch(e: ElectrumIncorrectRequest)
        {
        }

        val (name, ver) = cnxn.version(1000)
        LogIt.info("Server name $name, server version $ver")

        cnxn.call("server.banner", null) {
            LogIt.info("Server Banner reply is: " + it)
        }

        cnxn.parse("server.banner", null, BannerReply.serializer() ) {
            LogIt.info("Server Banner reply is: " + it!!.result)
        }

/*
        cnxn.subscribe("blockchain.headers.subscribe") {
            LogIt.info("Received blockchain header notification: ${it}")
        }
*/
        // Find the tip
        var tipHeight = -1
        cnxn.subscribeHeaders {
            tipHeight = it.height.toInt()
        }
        while (tipHeight == -1) sleep(200)

        val header = cnxn.getHeader(10000000)  // beyond the tip
        LogIt.info(header.toString())

        try
        {
            cnxn.getHeader(-1)  // beyond the tip
        }
        catch (e: ElectrumIncorrectRequest)
        {
            LogIt.info(e.toString())
        }
        LogIt.info(header.toString())


        // This code gets the first coinbase transaction and then checks its history.  Based on the normal regtest generation setup, there should be at least 100
        // blocks that generate to this same output.
        val firsttx = cnxn.getTxAt(1, 0)
        LogIt.info(firsttx.toHex())
        firsttx.debugDump()

        val txBlkHeader = BlockHeader(BCHserialized(cnxn.getHeader(1), SerializationType.HASH))

        // TODO: check server capabilities
        val firstUse = cnxn.getFirstUse(firsttx.outputs[0].script)
        LogIt.info("first use in block ${firstUse.block_hash}:${firstUse.block_height}, transaction ${firstUse.tx_hash}")
        check(firstUse.tx_hash != null)
        check(firstUse.block_height!! == 1)
        check(firsttx.hash.toHex() == firstUse.tx_hash!!)
        check(txBlkHeader.hash.toHex() == firstUse.block_hash)

        // doesn't exist
        try
        {
            val firstUse2 = cnxn.getFirstUse("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d00")
            assert(false)
        }
        catch(e: ElectrumNotFound)
        {
            // expected
        }

        // Test getUtxo

        // First check an unspent tx
        val lastcb = cnxn.getTxAt(tipHeight.toInt(), 0)
        val unspentUtxo = cnxn.getUtxo(lastcb.hash.toHex(), 0)
        LogIt.info(unspentUtxo.status)
        check(unspentUtxo.height == tipHeight)
        check(unspentUtxo.spent.height == null)
        check(unspentUtxo.status == "unspent")

        // next test getUtxo with a nonexistent tx
        try
        {
            val noTx = cnxn.getUtxo("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d00", 0)
            assert(false)
        }
        catch(e: ElectrumNotFound)
        {
            // expected
        }

        // Finally check a spent tx
        val result = cnxn.getUtxo(firsttx.hash.toHex(), 0)
        if (result.status != "spent")
        {
            LogIt.warning("This test assumes that you've spent the block 1 coinbase")
        }
        else
        {
            check(result.amount == 50*100*1000*1000L)
            check(result.height == 1)
            check(result.spent.height!! >= 101) // coinbases must be spent after 100 blocks
        }

        LogIt.info(result.status)


        val uses = cnxn.getHistory(firsttx.outputs[0].script)
        for (use in uses)
        {
            LogIt.info("used in block ${use.first} tx ${use.second}")
        }

        assert(uses.size >= 100)  // Might be wrong if the regtest chain startup is changed.

        val headers = cnxn.getHeaders(0, 1000, 10000)
        for (i in headers.indices)
        {
            val hdr = cnxn.getHeader(i, 1000)
            check(hdr contentEquals headers[i])

        }

        //Thread.sleep(20000)
        cnxn.close()
    }
}
