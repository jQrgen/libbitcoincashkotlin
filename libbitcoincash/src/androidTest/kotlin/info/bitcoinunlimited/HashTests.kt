package info.bitcoinunlimited.libbitcoincash
import bitcoinunlimited.libbitcoincash.*
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import kotlin.test.assertEquals


class HashTests {
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }

    @Test
    fun sha256() {
        val expected = "2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824"
        val result = UtilStringEncoding.toHexString(Hash.sha256("hello".toByteArray()))
        assertThat(expected).isEqualTo(result)
    }

    @Test
    fun testBCHhashing()
    {
        var hash1 = Hash.sha256(ByteArray(64, { _ -> 1 }))
        assertEquals("7c8975e1e60a5c8337f28edf8c33c3b180360b7279644a9bc1af3c51e6220bf5", hash1.toHex())
        var hash2 = Hash.hash256(ByteArray(64, { _ -> 1 }))
        assertEquals("61a088b4cf1f244e52e5e88fcd91b3b7d6135ebff53476ecc8436e23b5e7f095", hash2.toHex())
        var hash3 = Hash.hash160(ByteArray(64, { _ -> 1 }))
        assertEquals("171073b9bee66e89f657f50532412e3539bb1d6b", hash3.toHex())

        val data =
            "0100000001ef128218b638f8b34e125d3a87f074974522b07be629f84b72bba549d493abcb0000000049483045022100dbd9a860d31ef53b9ae12306f25a5f64ac732d5951e1d843314ced89d80c585b02202697cd52be31156a1c30f094aa388cb0d5e8e7f767472fd0b03cb1b7e666c23841feffffff0277800f04000000001976a9148132c3672810992a3c780772b980b1d690598af988ac80969800000000001976a914444b7eaa50459a727a4238778cde09a21f9b579a88ac81531400".fromHex()
        var hash4 = Hash256(Hash.hash256(data))
        assertEquals(hash4.toHex(), "1605af3f8beb87fa26fc12a45e52ce5e0e296d0da551c0775916634d451ca664")

        val tx = BCHtransaction(ChainSelector.BCHTESTNET, data, SerializationType.NETWORK)
        assertEquals(tx.hash.toHex(), hash4.toHex())

        // testnet3 block 1
        val blk = BCHblock(ChainSelector.BCHTESTNET,
            BCHserialized("0100000043497fd7f826957108f4a30fd9cec3aeba79972084e90ead01ea330900000000bac8b0fa927c0ac8234287e33c5f74d38d354820e24756ad709d7038fc5f31f020e7494dffff001d03e4b6720101000000010000000000000000000000000000000000000000000000000000000000000000ffffffff0e0420e7494d017f062f503253482fffffffff0100f2052a010000002321021aeaf2f8638a129a3156fbe7e5ef635226b0bafd495ff03afe2c843d7e3a4b51ac00000000".fromHex(),
                SerializationType.NETWORK))
        assertEquals(blk.hash.toHex(), "00000000b873e79784647a6c82962c70d228557d24a747ea4d1b8bbe878e1206")
    }
}
