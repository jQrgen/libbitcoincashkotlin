#!/usr/bin/env sh
set -ex
DIR="$(cd "$(dirname "$0")/.." && pwd)"
echo "Working directory: $DIR"

# Download previous repo builds and keep them.
$(wget --no-check-certificate $CI_PAGES_URL/repos_archive.tar.gz -O prev_repos.tar.gz || rm -f prev_repos.tar.gz);
if [ -f "prev_repos.tar.gz" ]; then
    tar -xzf prev_repos.tar.gz public/repos
fi

# Copy new stuff for deployment
mkdir -p public
rsync -a ./libbitcoincash/build/repos/ ./public/repos/
cp -r libbitcoincash/build/dokka/html ./public/dokka

# Tar all repos builds for next update
# TODO: Delete the oldest stuff before we reach gitlab pages limit.
tar -zcf public/repos_archive.tar.gz public/repos
