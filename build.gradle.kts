buildscript {
    val kotlinVersion by extra("1.5.31")
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath ("com.android.tools.build", "gradle", "7.0.2")
        classpath("org.jetbrains.kotlin", "kotlin-gradle-plugin", kotlinVersion)
        classpath("org.jetbrains.kotlin", "kotlin-serialization", kotlinVersion)
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }

}

task<Delete>("distclean") {
    delete(rootProject.buildDir)
    delete(File(projectDir,"contrib/build").absolutePath)
}

allprojects {
    repositories {
        google()
        mavenCentral()
        maven(url = "https://jitpack.io")
    }
}

plugins {
    idea
}

// Stop android studio from indexing the contrib folder
idea {
    module {
        excludeDirs.add(File(projectDir,"contrib"))
    }
}
